package org.example;

import com.google.gson.Gson;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class Producer {
    public void sendMessage(){
        // create properties
        String bootstrapServers = "127.0.0.1:9092";
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        // create producer
        KafkaProducer<String,String> first_producer = new KafkaProducer<String, String>(properties);
        // convert object to string
        Person person = new Person(1, "Pham Quang Anh", 23);
        String json = new Gson().toJson(person);
        // create producer record
        ProducerRecord<String, String> record = new ProducerRecord<String, String>("topic1", json);
        // sending the data
        first_producer.send(record, new Callback() {
            @Override
            public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                Logger logger = (Logger) LoggerFactory.getLogger(Producer.class);
                if(e==null){
                    logger.info("Successfully received the details as: \n" +
                    "Topic:" + recordMetadata.topic() + "\n" +
                            "Partition:" + recordMetadata.partition() + "\n" +
                            "Offset" + recordMetadata.offset() + "\n" +
                            "Timestamp" + recordMetadata.timestamp());
                }else{
                    logger.error("Can not produce, getting error", e);
                }
            }
        });
        first_producer.flush();
        first_producer.close();
    }

}
